/*
 *  Mr Pass.  Brain the size of a planet!
 *
 *  Proundly Created by Richard Buckland
 *  Share Freely Creative Commons SA-BY-NC 3.0. 
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "Game.h"
#include "mechanicalTurk.h"

int buildCampuses();
int mrSpinOff();
int convertGO8();
int buildArcs();

action decideAction (Game g) {

   
   action nextAction;

   nextAction.actionCode = PASS;

   int currentPlayer = getWhoseTurn(g);


   if (currentPlayer != NO_ONE) {
	   //get student numbers
	   int numMTV = getStudents(g, currentPlayer, STUDENT_MTV);
	   int numMMONEY = getStudents(g, currentPlayer, STUDENT_MMONEY);
	   int numMJ = getStudents(g, currentPlayer, STUDENT_MJ);
	   int numBPS = getStudents(g, currentPlayer, STUDENT_BPS);
	   int numTHD = getStudents(g, currentPlayer, STUDENT_THD);
	   int numBQN = getStudents(g, currentPlayer, STUDENT_BQN);
	   
	   //build campus when enough resourses
	   nextAction.actionCode = buildCampuses(numBPS, numBQN, numMJ, numMTV);
	   //decide spinoff
	   nextAction.actionCode = mrSpinOff(numMTV, numMMONEY, numMJ);
	   //convert to GO8
	   nextAction.actionCode = convertGO8(numMJ, numMMONEY);
	   //get arcs
	   nextAction.actionCode = buildArcs(numBPS, numBQN);


   }  
   return nextAction;
}

int convertGO8(int numMJ, int numMMONEY) {
	int result;
	if (numMJ >= 2 && numMMONEY >= 3) {
		result = BUILD_GO8;
	}
	return result;
}

int buildCampuses(int numBPS, int numBQN, int numMJ, int numMTV) {
	int result;
	if (numBPS >= 1 && numBQN >= 1 && numMJ >= 1 && numMTV >= 1) {
		result = BUILD_CAMPUS;
		numBPS--;
		numBQN--;
		numMJ--;
		numMTV--;
	}
	return result;
}

int mrSpinOff(int numMTV, int numMMONEY, int numMJ) {
	int result;
	if (numMTV >= 1 && numMMONEY >= 1 && numMJ >= 1) {
		result = START_SPINOFF;
		numMJ--;
		numMMONEY--;
		numMTV--;
	}
	return result;
}

int buildArcs(int numBPS, int numBQN) {
	int result;
	if (numBPS >= 1 && numBQN >= 1) {
		result = OBTAIN_ARC;
	}
	return result;
}