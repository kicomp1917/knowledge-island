Knowledge Island Repo
When making changes to the files in the repo, 
create another branch and upload the files into there.
Once you think you've got it working create a pull request
to the master branch and let others review your works.
Remember whenever you commit something, always put a clear
description on what that commit is about. It's standard 
engineering practice.
There is a continuous integration service available from Atlassian.
It basically compiles the files in real time and does almost 
all of the debugging for us, so if you think that's important 
I can set up a trial run for it. 
 
