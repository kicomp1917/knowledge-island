/*
runGame.c
Ben, Han, Will, Rohan
15/05/16
*/
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "Game.h"
#include "mechanicalTurk.h"

#include <time.h>
#include <assert.h>

//Can switch in different AIs instead of mrPass
//gcc -Wall -std=gnu99 -g -O -fno-omit-frame-pointer -lm runGame runGame.c mrPass.c Game.c
//This sample uses AI for uni a and b and human for player c

//If not all code done can at least compile this file
//but not end up with an executable
//gcc -Wall -std=gnu99 -g -O -fno-omit-frame-pointer -c runGame.c

//STILL ROUGH. Students NEED TO FIX FOR runGame.c activity
//Needs to get human move input
//print helpful output
//Make ways to run different boards (ie disciplines,dice permutations)
//Test it is right!! May be buggy? Use at own risk!


#define WINNING_KPI 150
#define FALSE 0
#define TRUE 1


#define DEFAULT_DISCIPLINES {STUDENT_BQN, STUDENT_MMONEY, STUDENT_MJ, \
    STUDENT_MMONEY, STUDENT_MJ, STUDENT_BPS, STUDENT_MTV, \
    STUDENT_MTV, STUDENT_BPS,STUDENT_MTV, STUDENT_BQN, \
    STUDENT_MJ, STUDENT_BQN, STUDENT_THD, STUDENT_MJ, \
    STUDENT_MMONEY, STUDENT_MTV, STUDENT_BQN, STUDENT_BPS}
    #define DEFAULT_DICE {9,10,8,12,6,5,3,11,3,11,4,6,4,7,9,2,8,10,5}

    int checkForWinner(Game g);
    int rollDice(int numRolls, int diceSides);
    action getMove(Game g);
    action getUserMove(Game g);

    int main(int argc, char * argv[]){
        int disciplines[] = DEFAULT_DISCIPLINES;
        int dice[] = DEFAULT_DICE;
        int turnOver = FALSE;
        int winner = NO_ONE;
        int diceValue;
        action move;
        int whoseTurn = NO_ONE;

        srand(time(NULL)); // Initialises a seed
        // Create g: pointer to the new game instance.
        Game g = newGame(disciplines,dice);



        while (winner == NO_ONE) {

            diceValue = rollDice(2,6);

            throwDice(g,diceValue);

            printf("[Turn %d] The turn now belongs to University %d!\n",
            getTurnNumber(g),
            getWhoseTurn(g));
            printf("The dice casted a %d!\n", diceValue);

            whoseTurn = getWhoseTurn(g);
            //loop until player action is PASS
            turnOver = FALSE;
            while (!turnOver) {
                move = getMove(g);
                //print info about move
                printf("Move %d \n",move.actionCode);
                assert(isLegalAction(g, move));

                if (move.actionCode == START_SPINOFF) {

                    // Decide if outcome is patent or publication
                    if (rand() % 3 <= 1) {
                        move.actionCode = OBTAIN_PUBLICATION;
                    } else {
                        move.actionCode = OBTAIN_IP_PATENT;
                    }
                }

                if(move.actionCode == PASS){
                    turnOver = TRUE;
                    printf("Pass\n");
                }
                makeAction(g, move);


                if(getKPIpoints(g, whoseTurn) >= WINNING_KPI){
                    winner = whoseTurn;
                    turnOver = TRUE;
                    printf("Winner %d\n",whoseTurn);
                }

            }
        }


        // TODO print game statistics:

        int uni = 0;
        while (uni < UNI_C) {
            // loop through unis and print their stats:
            printf("UNI %d's stats: ------\n", uni);
            printf("ARCS: %d\n", getKPIpoints(g, uni));
            printf("GO8 campuses: %d\n", getKPIpoints(g, uni));
            printf("IP's: %d\n", getKPIpoints(g, uni));
            printf("Publications: %d\n", getKPIpoints(g, uni));

            // Print the totals of each type of student from each uni,
            // sum the total number of students

            int student = 0;
            int totalStudents = 0;
            while (student < STUDENT_MMONEY){
                printf("Students: type %d = %d\n", student+1, getStudents(g,uni,student));
                totalStudents += getStudents(g,uni,student);
                student++;
            }

            printf("enromlents at UNI %d: %d\n", uni, totalStudents);

            printf("\n\nMOVES MADE: %d", getTurnNumber(g));
            printf("Final player making a move: uni %d\n", getWhoseTurn(g));
            uni++;
        }

        disposeGame(g);

        return EXIT_SUCCESS;
    }


    int checkForWinner(Game g) {
        int winner = NO_ONE;
        int playerIndex;

        playerIndex = UNI_A;
        while (playerIndex <= UNI_C) {
            // check if the player is over or equal the WINNING_KPI
            if (getKPIpoints(g, playerIndex) >= WINNING_KPI) {
                winner = playerIndex;
            }
            playerIndex++;
        }

        return winner;
    }

    int rollDice(int numRolls, int diceSides) {

        int diceRollCount = 0;
        int diceValue = 0;

        //"rolls the die"
        while (diceRollCount < numRolls) {
            diceValue += 1 + rand() % diceSides;
            diceRollCount++;
        }

        return diceValue;
    }

    //Just one possibility for mixing in AI and human
    //Students can modify however they like.
    action getMove(Game g){

        action move;
        if(getWhoseTurn(g) == UNI_A || getWhoseTurn(g) == UNI_B){
            move = decideAction(g); //AI
        } else {
            move = getUserMove(g);
        }
        return move;
    }

    // Gets next move from user:
    // Leave for them to do
    action getUserMove(Game g) {

        action move;

        //TODO user interaction
        printf("Please enter the actionCode for your move:\n");
        scanf("%d", &move.actionCode);

        if (move.actionCode >=1 && move.actionCode <=3) {
            // player wants to build a campus, GO8 campus or obtain an arc
            if (move.actionCode == 1 || move.actionCode == 2){
                printf("please enter the path of the campus you would like to build:\n");
                scanf("%s", move.destination);

            } else if (move.actionCode == 3) {
                printf("please enter the path to the arc you would like to obtain\n");
                scanf("%s", move.destination);
            }
            // we've now gotten the users desired action and path
            assert(isLegalAction(g, move) == 1); // check the user has inputted a legal move
            makeAction(g, move);


        } else if (move.actionCode >= 4 && move.actionCode <=6){
            // The user wishes to START_SPINOFF OBTAIN_PUBLICATION or OBTAIN_IP_PATENT

            isLegalAction(g,move);
            makeAction(g,move);

        } else if (move.actionCode == 7) {
            // the user wants to retrain students. We need to know who
            // they want to retrain first.
            printf("please enter the discipline you want to train from:\n");
            scanf("%d", &move.disciplineFrom);
            printf("please enter the discipline you want to train TO:\n");
            scanf("%d", &move.disciplineTo);

            isLegalAction(g, move);
            makeAction(g, move);

        }

        return move;
    }
