#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "Game.h"

#define DEFAULT_DISCIPLINES {STUDENT_BQN, STUDENT_MMONEY, STUDENT_MJ, \
                STUDENT_MMONEY, STUDENT_MJ, STUDENT_BPS, STUDENT_MTV, \
                STUDENT_MTV, STUDENT_BPS,STUDENT_MTV, STUDENT_BQN, \
                STUDENT_MJ, STUDENT_BQN, STUDENT_THD, STUDENT_MJ, \
                STUDENT_MMONEY, STUDENT_MTV, STUDENT_BQN, STUDENT_BPS}

#define DEFAULT_DICE {9,10,8,12,6,5,3,11,3,11,4,6,4,7,9,2,8,10,5}

void testInitialState(Game g, int *disciplines,int *dice);
void testTurn1State(Game g);

int main(int agrc, char *argv[]){
  int disciplines[] = DEFAULT_DISCIPLINES;
    int dice[] = DEFAULT_DICE;
    Game g = newGame (disciplines, dice);
    // disposeGame(g);
    testInitialState(g,disciplines,dice);
    throwDice(g,6);
    action a = {3,"R",6,6};
    makeAction(g,a);
    // disposeGame(g);
    action b = {3,"RR",6,6};
    makeAction(g,b);
    // disposeGame(g);
    testTurn1State(g);
    disposeGame(g);
    printf("All tests passed!\n");
    return EXIT_SUCCESS;
}

void testTurn1State(Game g){
    assert(getWhoseTurn(g) == UNI_A);
    assert(getTurnNumber(g) == 0);
    assert(getCampus(g,"RRLRL") == CAMPUS_B);
    assert(getARC(g,"R") == ARC_A);
    assert(getARC(g,"RR") == ARC_A);
}
//sample out testing simple things
void testInitialState(Game g ,int *disciplines,int *dice){
   printf("Test initialState!\n");

   //Just an example of how to use these functions
   int counter = 0;
   while (counter <= 18) {
       assert(getDiscipline(g,counter) == disciplines[counter]);
       assert(getDiceValue(g,counter) == dice[counter]);
       counter++;
   }

   assert(getMostARCs(g) == NO_ONE);
   assert(getMostPublications(g) == NO_ONE);
   assert(getTurnNumber(g) == -1);
   printf("whose turn = %d\n", getWhoseTurn(g));
   assert(getWhoseTurn(g) == NO_ONE);

   // Vertex tests
   assert(getCampus(g, "") == CAMPUS_A);
   assert(getCampus(g, "RRLRL") == CAMPUS_B); // Careful robot pointing INWARDS
   assert(getCampus(g, "LRLRL") == CAMPUS_C);
   assert(getCampus(g, "RLRL") == VACANT_VERTEX);
   assert(getCampus(g, "LRLRRLL") == VACANT_VERTEX);

   // ARC tests
   assert(getARC(g, "L") == VACANT_ARC);
   assert(getARC(g, "LR") == VACANT_ARC);
   assert(getARC(g, "LRR") == VACANT_ARC);
   assert(getARC(g, "LRRL") == VACANT_ARC);
   assert(getARC(g, "LRRLR") == VACANT_ARC);
   assert(getARC(g, "LRRLRL") == VACANT_ARC);
   assert(getARC(g, "LRRLRLRLR") == VACANT_ARC);

   assert(getARC(g, "R") == ARC_A);
   assert(getARC(g, "LRRLRLRLRLR") == ARC_A);

   assert(getARC(g, "RRLRLL") == ARC_B);
   assert(getARC(g, "LRLRLRRLRL") == ARC_B);

   assert(getARC(g, "LRLRL") == ARC_C);
   assert(getARC(g, "RLRLRLRRLR") == ARC_C);

   assert(getARC(g, "LRRLRL") == VACANT_ARC);
   assert(getARC(g, "LRRLRLRLR") == VACANT_ARC);

   int uni = UNI_A;

   while(uni <= UNI_C){
       assert(getKPIpoints(g,uni) == 20);
       assert(getARCs(g,uni) == 0);
       assert(getGO8s(g,uni) == 0);
       assert(getCampuses(g,uni) == 2);
       assert(getIPs(g,uni) == 0);
       assert(getPublications(g,uni) == 0);
    //    assert(getStudents (g,uni) == 0);

       int discipline = STUDENT_BPS;
       while(discipline <= STUDENT_MMONEY){
            assert(getExchangeRate(g,uni,discipline,STUDENT_MMONEY) == 3);
            discipline++;
       }
       uni++;
   }
   printf("passed!\n");
}
