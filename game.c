#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "Game.h"

#define TYPE_CAMPUS 0
#define TYPE_ARC 1

typedef struct _player {
   int numCAMP;
   int numARC;
   int numKPIs;
   int numGO8;
   int numPub;
   int numIPs;
   int numStudents[6];
} player;

struct _game {
   int turnNumber;
   int map[13][21];
   int diceNum;
   player players[3];

   // Pointers to player structs with the most Arcs and publications.
   player * mostArcs;
   player * mostPublications;
};

typedef struct _pos{
   int x;
   int y;
} pos;

/*
Returns pos struct with coords of:
CAMPUS if type is 0
ARC if type is 1
*/
static pos pathToCoord(path pathToVertex, char type) {

   assert(type <= 1);
   // Make sure we haven't been asked for an ARC with an empty path:
   assert(!(type == 1 && pathToVertex == '\0'));

   char currentd = 3; // 6 direction compass going from 1 ... -3 clockwise
   int i = 0; // counter for current string position
   char moveDistance = 2;// when returning an Arc we will chages this on last move

   pos coord = {5,0}; // coords of vertex at top left of top middle tile.

   while( pathToVertex[i] != '\0' && i < 150 ){
      // If we are returning an ARC, and we are currently reading the
      // last letter of the string, we are only going to move half way
      // (onto the arc not another vertex)
      if (type == 1 && pathToVertex[i+1] == '\0') {
         moveDistance = 1;
      }

      // Determine current direction, then update coords+dir from next letter
      if(currentd == 1){
         if (pathToVertex[i]=='R'){
            coord.x += moveDistance;
            currentd = 2;
         } else if (pathToVertex[i]=='L'){
            coord.y -= moveDistance;
            currentd = -3;
         } else if (pathToVertex[i]=='B'){
            coord.y += moveDistance;
            currentd = -1;
         }

      } else if(currentd == -1){
         if (pathToVertex[i]=='R') {
            coord.x -= moveDistance;
            currentd = -2;
         } else if (pathToVertex[i]=='L') {
            coord.y += moveDistance;
            currentd = 3;
         } else if (pathToVertex[i]=='B') {
            coord.y -= moveDistance;
         }

      } else if(currentd==2){
         if(pathToVertex[i]=='R'){
            coord.y += moveDistance;
            currentd = 3;
         } else if(pathToVertex[i]=='L'){
            coord.y -= moveDistance;
            currentd = 1;
         } else if(pathToVertex[i]=='B'){
            coord.x -= moveDistance;
            currentd = -2;
         }

      } else if(currentd==-2){
         if(pathToVertex[i]=='R') {
            coord.y -= moveDistance;
            currentd = -3;
         } else if (pathToVertex[i]=='L') {
            coord.y += moveDistance;
            currentd = -1;
         } else if (pathToVertex[i]=='B') {
            coord.x -= moveDistance;
            currentd = 2;
         }

      } else if(currentd==3){
         if(pathToVertex[i]=='R'){
            coord.y += moveDistance;
            currentd = -1;
         } else if (pathToVertex[i]=='L'){
            coord.x += moveDistance;
            currentd = 2;
         } else if (pathToVertex[i]=='B'){
            coord.y -= moveDistance;
         }

      } else if(currentd==-3){
         if(pathToVertex[i]=='R'){
            coord.y -= moveDistance;
            currentd = 1;
         } else if (pathToVertex[i]=='L'){
            coord.x -= moveDistance;
            currentd = -2;
         } else if (pathToVertex[i]=='B'){
            coord.y += moveDistance;
            currentd = 3;
         }
      }

      // We've updated the dir + coord, go to the next character in the string
      i++;
   }
   return coord;
}

static void giveStudents(Game g, int player, int studentType);

Game newGame(int discipline[], int dice[]) {
    Game g = (Game) malloc(sizeof(Game));
    int i = 0;
    int j = 0;
    int o = 0;
    //initialize the map
    while (i <= 12) { // 13 columns
        while (j <= 20) { // 21 rows

            if (i == 6) {
                if (j % 4 == 0) {
                    g->map[i][j] = 0;
                }
                if (j == 2 || j == 6 || j == 10 || j == 14 || j == 18) {
                    g->map[i][j] = o;
                    g->map[i][j - 1] = discipline[o];
                    g->map[i][j + 1] = dice[o];
                    o++;
                }

            }

            if (i == 5 || i == 7) {
                g->map[i][j] = 0;
            }

            if (i == 4 || i == 8) {
                if ((j - 2) % 4 == 0) {
                    g->map[i][j] = 0;
                } else if (j == 0 || j == 1 || j == 19 || j == 20) {
                    g->map[i][j] = -1;
                }
                if (j == 4 || j == 8 || j == 12 || j == 16) {
                    g->map[i][j] = o;
                    g->map[i][j - 1] = discipline[o];
                    g->map[i][j + 1] = dice[o];
                    o++;
                }

            }
            if (i == 3 || i == 9) {
                if (j == 0 || j == 1 || j == 19 || j == 20) {
                    g->map[i][j] = -1;
                } else {
                    g->map[i][j] = 0;
                }
            }

            if (i == 2 || i == 10) {
                if ((j - 4) < 16 && (j - 4) >= 0 && (j - 4) % 4 == 0) {
                    g->map[i][j] = 0;
                } else if ((j <= 4 && j >= 0) || (j <= 20 && j >= 17)) {
                    g->map[i][j] = -1;
                }
                if (j == 6 || j == 10 || j == 14) {
                    g->map[i][j] = o;
                    g->map[i][j - 1] = discipline[o];
                    g->map[i][j + 1] = dice[o];
                    o++;
                }
            }
            if (i == 1 || i == 11) {
                if ((j >= 0 && j <= 3) || (j <= 20 && j >= 17)) {
                    g->map[i][j] = -1;
                } else {
                    g->map[i][j] = 0;
                }
            }

            if (i == 0 || i == 12) {
                g->map[i][j] = -1;
            }

            j++;
        }
        j = 0;
        i++;
    }
    //initialize students for each players
    i = 0, j = 0;
    while (i <= 2) {
        g->players[i].numStudents[STUDENT_THD] = 0;
        g->players[i].numStudents[STUDENT_BPS] = 3;
        g->players[i].numStudents[STUDENT_BQN] = 3;
        g->players[i].numStudents[STUDENT_MJ] = 1;
        g->players[i].numStudents[STUDENT_MTV] = 1;
        g->players[i].numStudents[STUDENT_MMONEY] = 1;

        g->players[i].numCAMP += 2;
        g->players[i].numKPIs += 20;

        i++;
    }

    // Set the mostArcs and mostPublications pointers to NULL
    g->mostArcs = NULL;
    g->mostPublications = NULL;

    //initialize campus

    g->map[5][0] = 1;
    g->map[7][20] = 1;
    g->map[1][6] = 2;
    g->map[11][14] = 2;
    g->map[11][4] = 3;
    g->map[1][16] = 3;

    g->turnNumber = -1;
    return g;
}

void disposeGame (Game g){}

// Performs an action for a player
void makeAction (Game g, action a){
    int code = a.actionCode;

    player * currPlayer = &(g->players[getWhoseTurn(g)-1]); //

    if (code == PASS){
        return; // CHECK THIS!!!!
    } else if (code == BUILD_CAMPUS){
        // Get the vertex in our map from the path the player asked for
        pos coords = pathToCoord(a.destination, 0);
        // Test: Make sure this Vertex is currently vacant.
        if(g->map[coords.x][coords.y] == 0) {
           // Set the vertex to belong to the corresponding player.
           g->map[coords.x][coords.y] = getWhoseTurn(g);
       } else {
          printf("WARNING: Player attempted to build campus on non-vacant vertex\n");
       }
       currPlayer->numCAMP++;
       currPlayer->numKPIs += 10;

    } else if (code == BUILD_GO8){
        pos coords = pathToCoord(a.destination, 0);
        // Test: Check the player currently has a valid campus on this vertex.
        if (g->map[coords.x][coords.y] == getWhoseTurn(g)) {
           // make campus GO8
           g->map[coords.x][coords.y] = getWhoseTurn(g) + 3;
        } else {
           printf("WARNING: Player attempted to build GO8 Campus on vertex they do not own\n");
        }

        currPlayer->numKPIs += 10; // 20 KPIs for a GO8 campus, -10 for the lost normal campus.
        currPlayer->numGO8++;

    } else if (code == OBTAIN_ARC) {
        pos coords = pathToCoord(a.destination, 1);
        if (g->map[coords.x][coords.y] == VACANT_ARC) {
           g->map[coords.x][coords.y] = getWhoseTurn(g);
        } else {
           printf("WARNING: Player attempted to build ARC on non vacant ARC\n");
        }

        // Update the players ARC count:
        currPlayer->numARC++;
        currPlayer->numKPIs += 2; // +2 KPIs for each ARC owned.

        // See who has the most arcs at the moment so we can deduct points from
        // them if we need to change the new leader.
        int mostArcsPlayer = getMostARCs(g);

        // Check if this player now has the most Arcs and update mostArcs pointer if so.
        int uni = UNI_A;
        int hasMoreThan = 0;
        while (uni <= UNI_C) {
           if (currPlayer->numARC > g->players[uni-1].numARC) {
             hasMoreThan++;
          }
          uni++;
        }

        // If the uni getting the arc now has more arcs than both the other UNIs
        // AND they didnt already have the most arcs:
        if (hasMoreThan == 2 && mostArcsPlayer != getWhoseTurn(g)) {
           g->mostArcs = currPlayer; // Set the Uni with the most arcs pointer to this one.
           currPlayer->numKPIs += 10; // +10 points for having the most Arcs
           g->players[mostArcsPlayer-1].numKPIs -= 10; // Deduct the KPIs from the old leader.
        }

    } else if (code == OBTAIN_PUBLICATION) {
      // runGame already determined they are getting a Pub/IP patent, give to player:
      // Increment the number of publications owned by the player.
      // Update the players ARC count:
      int mostPublicationsPlayer = getMostPublications(g); // save old leader index
      currPlayer->numPub++;

      // Check if this player now has the most Arcs and update mostArcs pointer if so.
      int uni = UNI_A;
      int hasMoreThan = 0;
      while (uni <= UNI_C) {
         if (currPlayer->numPub > g->players[uni-1].numPub) {
           hasMoreThan++;
        }
        uni++;
      }

      if (hasMoreThan == 2 && mostPublicationsPlayer != getWhoseTurn(g)) {
         g->mostPublications = currPlayer; // Set the Uni with the most arcs pointer to this one.
         currPlayer->numKPIs += 10; // +10 points for having the most Arcs
         g->players[mostPublicationsPlayer-1].numKPIs -= 10; // Deduct the KPIs from the old leader.
     }

    } else if (code == OBTAIN_IP_PATENT) {
    // runGame already determined they are getting a Pub/IP patent, give to player:
    // Increment the number of IPS owned by the player.
    currPlayer->numKPIs += 10; // +10 KPIs for each IP.
    currPlayer->numIPs += 1;

    } else if (code == RETRAIN_STUDENTS) {
        int rate = getExchangeRate(g, getWhoseTurn(g), a.disciplineFrom, a.disciplineTo);
        // Swap the players students at calculated rate:
        currPlayer->numStudents[a.disciplineFrom] -= rate;
        currPlayer->numStudents[a.disciplineTo] += 1;
    }
}

static void giveStudents(Game g, int player, int studentType) {
    g->players[player-1].numStudents[studentType]++;
}

// advance the game to the next turn,
// assuming that the dice has just been rolled and produced diceScore
// the game starts in turn -1 (we call this state "Terra Nullis") and
// moves to turn 0 as soon as the first dice is thrown.
void throwDice (Game g, int diceScore){
    // This will get called when a user's next move is PASS

    assert(diceScore > 0 && diceScore <= 12);

    // Players with campuses on regions with dicevalue = diceScore will recieve
    // students corresponding to that regions student type
    // Find out which regions have the diceScore

    // TODO: IF A PLAYER HAS A GO8 CAMPUS ON THE REGION, THEY GET 2 STUDENTS OF
    // ITS TYPE, NOT JUST ONE!!!


    int regionID = 0;
    while (regionID < 19) {
        int diceVal = getDiceValue(g, regionID);
        if (diceVal == diceScore) {
            // Check the vertices of this region for campuses

            int i=0,j=0;
            while(j<=20){ // 21 Rows
                while(i<=12){ // 13 columns

                    if(g->map[i][j]!=-1 && (i-2)%4==0 && (j-2)%4==0){
                        if(g->map[i][j]==regionID){
                            if (g->map[i-1][j-2] != VACANT_VERTEX) {
                                giveStudents(g, g->map[i-1][j-2], g->map[i][j-1]);
                            } else if (g->map[i+1][j-2] != VACANT_VERTEX) {
                                giveStudents(g, g->map[i+1][j-2], g->map[i][j-1]);
                            } else if (g->map[i-1][j] != VACANT_VERTEX) {
                                giveStudents(g, g->map[i-1][j], g->map[i][j-1]);
                            } else if (g->map[i+1][j] != VACANT_VERTEX) {
                                giveStudents(g, g->map[i+1][j], g->map[i][j-1]);
                            } else if (g->map[i-1][j+2] != VACANT_VERTEX) {
                                giveStudents(g, g->map[i-1][j+2], g->map[i][j-1]);
                            } else if (g->map[i+1][j+2] != VACANT_VERTEX) {
                                giveStudents(g, g->map[i+1][j+2], g->map[i][j-1]);
                            }

                        }

                        if(g->map[i][j]!=-1 && (i==4||i==8) && j%4==0){
                            if(g->map[i][j]==regionID){
                                if (g->map[i-1][j-2] != VACANT_VERTEX) {
                                    giveStudents(g, g->map[i-1][j-2], g->map[i][j-1]);
                                } else if (g->map[i+1][j-2] != VACANT_VERTEX) {
                                    giveStudents(g, g->map[i+1][j-2], g->map[i][j-1]);
                                } else if (g->map[i-1][j] != VACANT_VERTEX) {
                                    giveStudents(g, g->map[i-1][j], g->map[i][j-1]);
                                } else if (g->map[i+1][j] != VACANT_VERTEX) {
                                    giveStudents(g, g->map[i+1][j], g->map[i][j-1]);
                                } else if (g->map[i-1][j+2] != VACANT_VERTEX) {
                                    giveStudents(g, g->map[i-1][j+2], g->map[i][j-1]);
                                } else if (g->map[i+1][j+2] != VACANT_VERTEX) {
                                    giveStudents(g, g->map[i+1][j+2], g->map[i][j-1]);
                                }


                            }

                            i++;
                        }
                        i=0;
                        j++;
                    }
                    regionID++;
                }


            }

            // Work out which campuses are on the vertices of that region

            // Give player students of correct type for that region


            g->turnNumber += 1;
        }
    }
}

int getDiscipline (Game g, int regionID){
   assert(g != NULL);

   int diceValue=0;
   int i=0,j=0;
   while(j<=20){
      while(i<=12){
         if(g->map[i][j]!=-1 && (i-2)%4==0 &&(j-2)%4==0 ){
            if(g->map[i][j]==regionID){
               diceValue=g->map[i][j-1];
            }
         }
         if(g->map[i][j]!=-1&&(i==4||i==8)&&j%4==0){
            if(g->map[i][j]==regionID){
               diceValue=g->map[i][j-1];
            }

         }
         i++;
      }
      i=0;
      j++;
   }
   return diceValue;

}

int getDiceValue (Game g, int regionID){
   assert(g != NULL);

   int diceValue=0;
   int i=0,j=0;
   while(j<=20){ // 21 Rows
      while(i<=12){ // 13 columns

         if(g->map[i][j]!=-1 && (i-2)%4==0 && (j-2)%4==0){
            if(g->map[i][j]==regionID){
               diceValue=g->map[i][j+1];
            }
         }

         if(g->map[i][j]!=-1 && (i==4||i==8) && j%4==0){
            if(g->map[i][j]==regionID){
               diceValue=g->map[i][j+1];
            }

         }

         i++;
      }
      i=0;
      j++;
   }
   return diceValue;
}

int getMostARCs (Game g){
   assert(g != NULL);

   unsigned char uni = UNI_A;
   unsigned char uniToReturn = NO_ONE;

   while (uni < UNI_C) { // For each uni
      if (&g->players[uni-1] == g->mostArcs) { // If the mostArcs pointer points to this uni
         // Save int result for that uni and update mostArcs
         uniToReturn = uni;
      }
      uni++;
   }

   return uniToReturn;
}

int getMostPublications (Game g){
    assert(g != NULL);

    unsigned char uni = UNI_A;
    unsigned char uniToReturn = NO_ONE;

    while (uni < UNI_C) { // For each uni
       if (&g->players[uni-1] == g->mostPublications) { // If the mostPublications pointer points to this uni
          // Save int result for that uni and update mostPublications
          uniToReturn = uni;
       }
       uni++;
    }

    return uniToReturn;
}

int getTurnNumber (Game g){
   assert(g != NULL);
   return g->turnNumber;

}

int getWhoseTurn (Game g){
   assert(g != NULL);
   if (g->turnNumber == -1) {
       return NO_ONE;
   } else {
   return g->turnNumber%3;
   }
}

int getCampus(Game g, path pathToVertex){
   assert(g != NULL);

   // Call our function to get us the coordinates on the map from a path
   // int 0 argument returns campus coord instead of arc
   pos tempPos = pathToCoord(pathToVertex, TYPE_CAMPUS);

   // Now we are going to check and return the contents of the campuses coord:
   assert(g->map[tempPos.x][tempPos.y] <= 6); // Quick check to make sure contents of the
   // coordinate is valid.
   return g->map[tempPos.x][tempPos.y];

}

int getARC(Game g, path pathToEdge){
   assert(g != NULL);

   pos tempPos = pathToCoord(pathToEdge, TYPE_ARC);

   assert(g->map[tempPos.x][tempPos.y] <= 3); // Quick check to make sure contents of the
   // coordinate is valid.
   return g->map[tempPos.x][tempPos.y];
}

int isLegalAction (Game g, action a){

    return 0;

}

int getKPIpoints (Game g, int player){
   assert(g != NULL);
   return g->players[player-1].numKPIs;
   // Should we actually be calculating the KPI's here? Where is numKPIs getting incremeted from?
}

int getARCs (Game g, int player){
   assert(g != NULL);
   return g->players[player-1].numARC;
}

int getGO8s (Game g, int player){
   assert(g != NULL);
   return g->players[player-1].numGO8;
}

int getCampuses (Game g, int player){
   assert(g != NULL);
   return g->players[player-1].numCAMP;
}

int getIPs (Game g, int player) {
   assert(g != NULL);
   return g->players[player-1].numIPs;

}

int getPublications (Game g, int player) {
   assert(g != NULL);
   return g->players[player-1].numPub;

}

int getStudents ( Game g,  int player, int discipline) {
   assert(g != NULL);
   return g->players[player-1].numStudents[discipline];
}

int getExchangeRate (Game g, int player, int disciplineFrom, int disciplineTo) {
   return 0;
}
