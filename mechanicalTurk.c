/*
 *  Mr Pass.  Brain the size of a planet!
 *
 *  Proundly Created by Richard Buckland
 *  Share Freely Creative Commons SA-BY-NC 3.0. 
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "Game.h"
#include "mechanicalTurk.h"

action decideAction (Game g) {

   
   action nextAction;

   nextAction.actionCode = PASS;

   int currentPlayer = getWhoseTurn(g);


   if (currentPlayer != NO_ONE) {
	   //get student numbers
	   int numMTV = getStudents(g, currentPlayer, STUDENT_MTV);
	   int numMMONEY = getStudents(g, currentPlayer, STUDENT_MMONEY);
	   int numMJ = getStudents(g, currentPlayer, STUDENT_MJ);
	   int numBPS = getStudents(g, currentPlayer, STUDENT_BPS);
	   int numTHD = getStudents(g, currentPlayer, STUDENT_THD);
	   int numBQN = getStudents(g, currentPlayer, STUDENT_BQN);
	   int numGO8 = 0;
	   int player = 0;

	   while (player < 2) {
		   numGO8 += getGO8s(g, player);
		   player++;
	   }
	   //convert to GO8
	   if (numGO8 < 8) {
		   if (numMJ >= 2 && numMMONEY >= 3) {
			   nextAction.actionCode = BUILD_GO8
			   //nextAction.destination = 
			   numMJ--;
			   numMMONEY--;
		   }
	   }
	   //build campus when enough resourses
	   if (numMTV >= 1 && numBPS >= 1 && numBQN >= 1 && numMJ >= 1) {
		   nextAction.actionCode = BUILD_CAMPUS;
		   //nextAction.destination = 
		   numBPS--;
		   numBPS--;
		   numMJ--;
		   numMTV--;
	   }
	   //decide spinoff
	   if (numMTV >= 1 && numMMONEY >= 1 && numMJ >= 1) {
		   nextAction.actionCode = START_SPINOFF;
		   numMTV--;
		   numMMONEY--;
		   numMJ--;
	   }
	   //get arcs
	   if (numBPS >= 1 && numBQN >= 1) {
		   nextAction.actionCode = OBTAIN_ARC;
		   //nextAction.destination = 
		   numBPS--;
		   numBQN--;
	   }
   }  
   return nextAction;
}
